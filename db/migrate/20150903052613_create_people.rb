class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.date :DOB
      t.string :Name
      t.datetime :CreatedAt

      t.timestamps null: false
    end
  end
end
