#Heroku Deployment
##New
1. Clone git repo
2. Navigate to repository folder and run command to create an app on heroku with the the name you supply it
> heroku create {name}
3. using git push the repository to heroku with 
> git push heroku master && heroku run rake db:migrate && heroku restart
##Existing
1. Clone git repo
2. Navigate to repository folder and run command to create remotes in your local git repo that you can push your code to. in this case the remote is the named git on heroku.com
> git remote add heroku git@heroku.com:{name}.git
3. using git push the repository to heroku with 
> git push heroku master && heroku run rake db:migrate && heroku restart

###NOTE
when pushing this project to heroku for v1 and v2 you can push the different branches by checking out the v1 or v2 branch and then using the command "git push heroku {branch}:master"



#How to get Vagrant development vm working

1. clone the git repo to your local machine

2. install pre-requisite roles for ansible
> ansible-galaxy install -r ansible-requirements.yml

3. run command to build and run the vagrant box
> vagrant up

4. connect to the devbox by ssh 
> vagrant ssh 

5. navigate to the rails project folder
> cd /PersonAge

6. run the rails dependency installer
> bundle install

7. if/when needed run the rails server
> rails server -b 0.0.0.0