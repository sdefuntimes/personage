json.array!(@people) do |person|
  json.extract! person, :id, :DOB, :Name, :CreatedAt
  json.url person_url(person, format: :json)
end
